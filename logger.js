const winston = require('winston');
const moment = require('moment');
const util = require('util');
const path = require('path');
const MESSAGE = Symbol.for('message');

module.exports = (filename) => winston.createLogger({
  level: 'debug',
  format: winston.format.combine(
    winston.format(function(info, opts) {
      prefix = util.format(
        '[%s] [%s] [%s]',
        moment().format('YYYY-MM-DD hh:mm:ss').trim(),
        info.level.toUpperCase(),
        path.basename(filename)
      );
      if (info.splat) {
        info.message = util.format('%s %s', prefix, util.format(info.message, ...info.splat));
      } else {
        info.message = util.format('%s %s', prefix, info.message);
      }
      return info;
    })(),
    winston.format(function(info) {
      info[MESSAGE] = info.message + ' ' + JSON.stringify(
        Object.assign({}, info, {
          level: undefined,
          message: undefined,
          splat: undefined
        })
      );
      return info;
    })()
  ),
  transports: [
    new winston.transports.Console({
      format: winston.format.simple()
    })
  ]
});