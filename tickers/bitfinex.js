const WebSocket = require('ws');
const request = require('request');
const logger = require('../logger')(__filename);

module.exports.name = 'Bitfinex';

module.exports.connect = (state) => {
  var socket;
  const pairs = Object.keys(state);
  var channels = {};

  function reconnect() {
    socket = new WebSocket('wss://api.bitfinex.com/ws/2');

    socket.on('open', () => {
      pairs.forEach((pair) => {
        logger.log('debug', `subscribing to ${pair}`);
        socket.send(JSON.stringify({
          event: 'subscribe',
          channel: 'ticker',
          pair: pair
        }));
      });
    });

    socket.on('message', (message) => {
      message = JSON.parse(message);

      if (Array.isArray(message)) {
        if (Array.isArray(message[1])) {
          state[channels[message[0]]] = message[1][6];
        }
      } else if (message.event == 'subscribed') {
        channels[message.chanId] = message.pair;
      }
    });

    socket.on('error', (error) => {
      logger.log('error', error);

      Object.keys(pair).forEach((pair) => {
        state[pair] = null;
      });

      setTimeout(() => {
        logger.log('debug', 'reconnecting');
        reconnect();
      }, 500);
    });

    socket.on('close', () => {
      logger.log('connection lost');

      Object.keys(pair).forEach((pair) => {
        state[pair] = null;
      });

      setTimeout(() => {
        logger.log('debug', 'reconnecting');
        reconnect();
      }, 500);
    });
  }

  reconnect();

  pairs.forEach((pair) => {
    logger.log('debug', `fetching pair ${pair}`);
    request.get({
      url: `https://api.bitfinex.com/v1/pubticker/${pair}`,
      json: true
    }, (error, response, body) => {
      if (error) {
        logger.log('error', error);
        return;
      }

      state[pair] = body.last_price;
    });
  })
}