const WebSocket = require('ws');
const request = require('request');
const logger = require('../logger')(__filename);

const TICKER_CHANNEL = 1002;

module.exports.name = 'Poloniex';

module.exports.connect = (state) => {
  var socket;
  const pairs = Object.keys(state);
  var channels = {};

  function reconnect() {
    socket = new WebSocket('wss://api2.poloniex.com');

    socket.on('open', () => {
      socket.send(JSON.stringify({
        command: 'subscribe',
        channel: TICKER_CHANNEL
      }));
    });

    socket.on('message', (message) => {
      message = JSON.parse(message);

      if (message[0] == TICKER_CHANNEL && Array.isArray(message[2]) && channels.hasOwnProperty(message[2][0])) {
        state[channels[message[2][0]]] = message[2][1];
      }
    });

    socket.on('error', (error) => {
      logger.log('error', error);

      Object.keys(pair).forEach((pair) => {
        state[pair] = null;
      });

      setTimeout(() => {
        logger.log('debug', 'reconnecting');
        reconnect();
      }, 500);
    });

    socket.on('close', () => {
      logger.log('connection lost');

      Object.keys(pair).forEach((pair) => {
        state[pair] = null;
      });

      setTimeout(() => {
        logger.log('debug', 'reconnecting');
        reconnect();
      }, 500);
    });
  }

  logger.log('debug', 'fetching initial data');
  
  request.get({
    url: 'https://poloniex.com/public?command=returnTicker',
    json: true
  }, (error, response, body) => {
    if (error) {
      logger.log('error', error);
      return;
    }

    Object.keys(body).forEach((pair) => {
      if (!state.hasOwnProperty(pair)) return;
      channels[body[pair].id] = pair;
      state[pair] = body[pair].last;
    });
  });
}