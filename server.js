const path = require('path');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const config = require('./config');
const logger = require('./logger')(__filename);

var state = {};
const tickersPath = path.join(__dirname, 'tickers');

fs.readdirSync(tickersPath).forEach(function(file) {
  const ticker = require(path.join(tickersPath, file));

  if (!config.exchanges.hasOwnProperty(ticker.name)) return;

  logger.log('debug', `loading ticker ${file}`);

  var tickerState = state[ticker.name] = {};
  config.exchanges[ticker.name].forEach((pair) => tickerState[pair] = null);
  ticker.connect(tickerState);
});


const app = express();
app.use(bodyParser.json());
app.post('/', (request, response) => {
  if (request.query.token != config.token) {
    response.sendStatus(401);
    return;
  }

  var data = {};
  Object.keys(request.body).forEach((name) => {
    if (state.hasOwnProperty(name)) {
      data[name] = {};
      request.body[name].forEach((pair) => {
        data[name][pair] = state[name][pair];
      });
    }
  });
  response.send(data);
});
app.use((error, request, response, next) => {
  if (error) {
    response.sendStatus(400);
    return;
  }

  next();
});
app.listen(process.env.PORT || config.port);